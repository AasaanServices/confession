<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%-- <%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %> --%>
<c:set var="context" value="${pageContext.request.contextPath}" />
 
<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
   <jsp:include page="common/innerheadimport.jsp" />
   <!-- Plugin -->
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/datatables-bootstrap/dataTables.bootstrap.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/datatables-responsive/dataTables.responsive.css">
   <style>
    @media (max-width: 480px) {
      .panel-actions .dataTables_length {
        display: none;
      }
    }
    
    @media (max-width: 320px) {
      .panel-actions .dataTables_filter {
        display: none;
      }
    }
    
    @media (max-width: 767px) {
      .dataTables_length {
        float: left;
      }
    }
    
    #exampleTableAddToolbar {
      padding-left: 30px;
    }
  </style>
  
</head>
<body class="dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <jsp:include page="common/header.jsp"></jsp:include>
  
    <jsp:include page="common/menu.jsp"/>

  
  

  <!-- Page -->
  <div class="page">
  
   <div class="page-header">
      <h1 class="page-title"><i class="site-menu-icon wb-users" aria-hidden="true"></i>Manage Users</h1>
      <ol class="breadcrumb">
        <li><a href="${context}/sa/dashboard">Dashboard</a></li>
        <li class="active">Manage Users</li>
      </ol>
    </div>
    
    
    
    <div class="page-content container-fluid">
     
     <div class="panel">
     
      <header class="panel-heading">
          <h3 class="panel-title">
            <a class="btn btn-primary" href="${context}/sa/userCU?action=c" style="color:#fff;">Add New</a>
            
          </h3>
        </header>
        
        <div class="panel-body container-fluid">
          <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
              <tr>
                <th>User Name</th>
                <th>User Email</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
              <tr>
                <th>User Name</th>
                <th>User Email</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>
               <c:if test="${fn:length(usersList) > 0}">
                      <c:forEach items="${usersList}" var="element" varStatus="loop"> 
                        <tr>
                          <td id="email${element.id}">${element.name}</td>
                         
                          <td id="email${element.id}">${element.email}</td>
                          
                           <td id="email${element.id}">${element.craetedDate}</td>
                           
                          
                          
                          <td id="status${element.id}">
  							<c:if test="${element.active eq 'A'}">
		                          <span class="label label-success" id="cStatus${element.id}">Active</span>
		                    </c:if>
		                    <c:if test="${element.active eq 'I'}">
		                          <span class="label label-danger" id="cStatus${element.id}">Inactive</span>
		                    </c:if>
						  </td>
						  
						  
						   <td id="cService${element.id}">
		                         <c:forEach items="${element.roles}" var="service"> 
		                            <a href="${context}/sa/servicesCU?action=u&token=${service.id}"><span class="label label-primary">${service.type}</span></a>
		                        </c:forEach>
	                        </td>
						   
                          
                          <td id="action${element.id}"><a  class="btn btn-primary btn-sm" href="${context}/sa/userCU?action=u&token=${element.id}"><i class="fa fa-edit"></i> Edit</a>
                          
                          
                           </td>
                          
              			</tr>
                      </c:forEach>
               </c:if>
            </tbody>
          </table>
        </div>
      </div>
      
      
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <jsp:include page="common/footer.jsp"></jsp:include>

  <jsp:include page="common/innerbottomimport.jsp" />
  
  <script src="${context}/resources/app/assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="${context}/resources/app/assets/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="${context}/resources/app/assets/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
  <script src="${context}/resources/app/assets/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="${context}/resources/app/assets/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
  <script src="${context}/resources/app/assets/js/components/datatables.js"></script>
  
  <script>
    $(document).ready(function($) {
      Site.run();
      $("#saManageUsers").addClass("active");
    });
  </script>
  
  
  
  
</body>

</html>