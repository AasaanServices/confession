<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
   <jsp:include page="common/innerheadimport.jsp" />
  <style>
	  .error
	  {
	     color: #f96868;
	  }
   </style>
</head>
<body class="dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <jsp:include page="common/header.jsp"></jsp:include>
  
    <jsp:include page="common/menu.jsp"/>

  
  

  <!-- Page -->
  <div class="page">
  
   <div class="page-header">
      <h1 class="page-title"><i class="site-menu-icon wb-users" aria-hidden="true"></i>Manage Users</h1>
      <ol class="breadcrumb">
        <li><a href="${context}/sa/dashboard">Dashboard</a></li>
        <li class="active">Manage Users</li>
      </ol>
    </div>
    
    
    
    <div class="page-content container-fluid">
      <form:form action="${context}/sa/saveUser" modelAttribute="webUserDTO" method="POST">
      		  <form:hidden path="action"/>
              <form:hidden path="id"/>
      <div class="panel">
      
        <header class="panel-heading">
          <h3 class="panel-title">
            <c:if test="${webUserDTO.action eq 'c'}">
                <span style="margin-right: 20px;"><strong>Add Record</strong></span> 
              </c:if>
              <c:if test="${webUserDTO.action eq 'u'}">
                <span style="margin-right: 20px;"><strong>Update Record</strong></span> 
              </c:if>
            <a class="btn btn-primary" href="${context}/sa/listUser" style="color:#fff;">Users List</a>
          </h3>
        </header>
        
        
         <div class="panel-body container-fluid">
 
 
          <div class="row row-lg">
          
          
		          <div class="col-sm-6">
		            <div class="example-wrap">
		                <div class="example">
		                 	
		                 	<div class="form-group">
		                      <label class="control-label">First Name <sup style="color: #dd4b39;">*</sup></label>
		                      <form:input path="firstName" class="form-control focusfield"/>
		                  	  <form:errors path="firstName" cssClass="error" /> 
		                    </div>
		                    
		                    <div class="form-group">
		                      <label class="control-label">Last Name <sup style="color: #dd4b39;">*</sup></label>
		                      <form:input path="lastName" class="form-control"/>
		                      <form:errors path="lastName" cssClass="error" />
		                    </div>
		                    
		                    <div class="form-group">
		                      <label class="control-label">User Email <sup style="color: #dd4b39;">*</sup></label>
		                      <form:input path="email" class="form-control"/>
		                      <form:errors path="email" cssClass="error" />
		                    </div>
		                     <div class="form-group">
		                      <label class="control-label">User Password <sup style="color: #dd4b39;">*</sup></label>
		                      <form:input path="password" class="form-control"/>
		                      <form:errors path="password" cssClass="error" />
		                    </div>
		                    
		                    
		                    
		                    <div class="form-group">
		                      <label class="control-label">Status <sup style="color: #dd4b39;">*</sup></label>
		                      <form:select path="status" class="form-control selectpicker show-tick" data-live-search="true">
			                      <form:option value="A" selected="selected">Active</form:option>
			                      <form:option value="I">Inactive</form:option>
			                  </form:select>
		                    </div>
		                   
		                    
		                        <div class="example-wrap">
		                <div class="example">
		                  <div class="box-footer">
			                  <c:if test="${msg ne null}">
								  	<p style="color: #00a65a;"><strong>Success!</strong> ${msg}</p>
				              </c:if>
				              <p style="color: #dd4b39;">* Mandatory Fields</p>
				              <c:if test="${webUserDTO.action eq 'c'}">
				                <button type="submit" class="btn btn-primary" id="submitBtn"><i class="fa fa-plus"></i> Add</button>
				              </c:if>
				              <c:if test="${webUserDTO.action eq 'u'}">
				                <button type="submit" class="btn btn-primary" id="submitBtn"><i class="fa fa-history"></i> Update</button>
				              </c:if>
				              <button type="button" class="btn btn-primary" id="resetBtn"><i class="fa fa-refresh"></i> Reset</button>
				              
             			 </div>
		                </div>
		            </div> 
		            
		            
		            
		                    
		                </div>
		               </div>
		           </div>
		           
		           
		           
                
              </div>
             
            
            
		</div></div>
		 </form:form>
       
		</div>
            
      </div>   
  <!-- End Page -->


  <!-- Footer -->
  <jsp:include page="common/footer.jsp"></jsp:include>

  <jsp:include page="common/innerbottomimport.jsp" />
  
  <script>
    $(document).ready(function($) {
      Site.run();
      $("#saUserCU").addClass("active");
      $('.selectpicker').selectpicker();
      $("#resetBtn").click(function(){
 		 window.location.replace('${context}'+'/sa/userCU?action=c');
 	 });
      
    });
  </script>
  
  
  
  
</body>

</html>