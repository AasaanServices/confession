<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
 
 
  <!-- Core  -->
  <script src="${context}/resources/app/assets/vendor/jquery/jquery.js"></script>
  <script src="${context}/resources/app/assets/vendor/bootstrap/bootstrap.js"></script>
  <script src="${context}/resources/app/assets/vendor/animsition/jquery.animsition.js"></script>
  <script src="${context}/resources/app/assets/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="${context}/resources/app/assets/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="${context}/resources/app/assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="${context}/resources/app/assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

  <!-- Plugins -->
  <script src="${context}/resources/app/assets/vendor/switchery/switchery.min.js"></script>
  <script src="${context}/resources/app/assets/vendor/intro-js/intro.js"></script>
  <script src="${context}/resources/app/assets/vendor/screenfull/screenfull.js"></script>
  <script src="${context}/resources/app/assets/vendor/slidepanel/jquery-slidePanel.js"></script>

  <script src="${context}/resources/app/assets/vendor/chartist-js/chartist.min.js"></script>
  <script src="${context}/resources/app/assets/vendor/aspieprogress/jquery-asPieProgress.min.js"></script>
  <script src="${context}/resources/app/assets/vendor/matchheight/jquery.matchHeight-min.js"></script>

  <!-- Scripts -->
  <script src="${context}/resources/app/assets/js/core.js"></script>
  <script src="${context}/resources/app/assets/js/site.js"></script>

  <script src="${context}/resources/app/assets/js/sections/menu.js"></script>
  <script src="${context}/resources/app/assets/js/sections/menubar.js"></script>
  <script src="${context}/resources/app/assets/js/sections/sidebar.js"></script>

  <script src="${context}/resources/app/assets/js/configs/config-colors.js"></script>
  <script src="${context}/resources/app/assets/js/configs/config-tour.js"></script>

  <script src="${context}/resources/app/assets/js/components/asscrollable.js"></script>
  <script src="${context}/resources/app/assets/js/components/animsition.js"></script>
  <script src="${context}/resources/app/assets/js/components/slidepanel.js"></script>
  <script src="${context}/resources/app/assets/js/components/switchery.js"></script>
  <script src="${context}/resources/app/assets/js/components/matchheight.js"></script>
  <script src="${context}/resources/app/assets/js/custom/commonvalidation.js"></script>
  
  <script src="${context}/resources/app/assets/vendor/summernote/summernote.min.js"></script>
  <script src="${context}/resources/app/assets/js/components/summernote.js"></script>
  <script src="${context}/resources/app/assets/bootstrap-select/bootstrap-select.min.js"></script>
    