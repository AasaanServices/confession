<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<spring:url value="/u/like" var="like"/>
<spring:url value="/u/dislike" var="dislike"/>
<spring:url value="/u/post" var="post"/>
<spring:url value="/u/upost" var="upost"/>
<spring:url value="/u/dpost" var="dpost"/>
<spring:url value="/u/comment" var="commen"/>

<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="${context}/resources/bootstrap.min.css" rel="stylesheet">
  <link href="${context}/resources/bootstrap.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body{
    margin-top:0px;
    background:white;
	padding-left:10px;
	padding-right:10px;
}

.img-sm {
    width: 46px;
    height: 46px;
}

.panel {
    box-shadow: 0 2px 0 rgba(0,0,0,0.075);
    border-radius: 0;
    border: 0;
    margin-bottom: 15px;
}

.panel .panel-footer, .panel>:last-child {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}

.panel .panel-heading, .panel>:first-child {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}

.panel-body {
    padding: 25px 20px;
}


.media-block .media-left {
    display: block;
    float: left
}

.media-block .media-right {
    float: right
}

.media-block .media-body {
    display: block;
    overflow: hidden;
    width: auto
}

.middle .media-left,
.middle .media-right,
.middle .media-body {
    vertical-align: middle
}

.thumbnail {
    border-radius: 0;
    border-color: #e9e9e9
}

.tag.tag-sm, .btn-group-sm>.tag {
    padding: 5px 10px;
}

.tag:not(.label) {
    background-color: #fff;
    padding: 6px 12px;
    border-radius: 2px;
    border: 1px solid #cdd6e1;
    font-size: 12px;
    line-height: 1.42857;
    vertical-align: middle;
    -webkit-transition: all .15s;
    transition: all .15s;
}
.text-muted, a.text-muted:hover, a.text-muted:focus {
    color: #acacac;
}
.text-sm {
    font-size: 0.9em;
}
.text-5x, .text-4x, .text-5x, .text-2x, .text-lg, .text-sm, .text-xs {
    line-height: 1.25;
}

.btn-trans {
    background-color: transparent;
    border-color: transparent;
    color: #929292;
}

.btn-icon {
    padding-left: 9px;
    padding-right: 9px;
}

.btn-sm, .btn-group-sm>.btn, .btn-icon.btn-sm {
    padding: 5px 10px !important;
}

.mar-top {
    margin-top: 15px;
}
select.list1
{
    background-color: #00CED1;
	padding-left: 5px;
	color:white;
}
.panel .panel-footer i.glyphicon-thumbs-up { color: #1abc9c; }
.panel .panel-footer i.glyphicon-thumbs-down { color: #e74c3c; padding-left: 5px; }
.dropdown {
    display:inline-block;
    margin-left:20px;
    padding:10px;
  }


.glyphicon-bell {
   
    font-size:1.5rem;
  }

.notifications {
   min-width:420px; 
  }
  
  .notifications-wrapper {
     overflow:auto;
      max-height:250px;
    }
    
 .menu-title {
     color:#ff7788;
     font-size:1.5rem;
      display:inline-block;
      }
 
.glyphicon-circle-arrow-right {
      margin-left:10px;     
   }
  
   
 .notification-heading, .notification-footer  {
 	padding:2px 10px;
       }
      
        
.dropdown-menu.divider {
  margin:5px 0;          
  }

.item-title {
  
 font-size:1.3rem;
 color:#000;
    
}

.notifications a.content {
 text-decoration:none;
 background:#ccc;

 }
    
.notification-item {
 padding:10px;
 margin:5px;
 background:#ccc;
 border-radius:4px;
 }
 .white {
    color: white;
}
.navbar-nav>li>a.profile-image {
    padding-top: 10px;
    padding-bottom: 10px;
}
.navbar-fixed-left {
  width: 100%;
 
  border-radius: 0;
  height: 250px;;
}

.navbar-fixed-left .navbar-nav > li {
  float: none;  /* Cancel default li float: left */
  width: 240px;
}
</style>
</head>
<body>




<nav class="navbar navbar-default" style="background-color:#00CED1;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><p style="font-size:200%;color:white;"><b>CONFESSION</b></p></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      <form class="navbar-form navbar-left" style="width:600px;">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search" style="width:400px;">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
<!-- ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc -->
  <ul class="nav navbar-nav navbar-right">
     <div class="dropdown">
  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    <i class="glyphicon glyphicon-bell white"></i>
  </a>
  
  <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
    
    <div class="notification-heading"><h4 class="menu-title">Notifications</h4><h4 class="menu-title pull-right">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
    </div>
    <li class="divider"></li>
   <div class="notifications-wrapper">
     <a class="content" href="#">
      
       <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 · day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
       
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 · day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>

    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">Evaluation Deadline 1 • day ago</h4>
        <p class="item-info">Marketing 101, Video Assignment</p>
      </div>
    </a>

   </div>
   
    <li class="divider"></li>
    <div class="notification-footer"><h4 class="menu-title">View all<i class="glyphicon glyphicon-circle-arrow-right"></i></h4></div>
  </ul>
  
</div><!-- /.end notification -->
<!-- /.settingllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll -->
   <div class="dropdown">
  <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    <i class="glyphicon glyphicon-cog white"></i>
  </a>
  
  <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
    
    <div class="notification-heading"><h4 class="menu-title">setting</h4><h4 class="menu-title pull-right">setting<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
    </div>
    <li class="divider"></li>
   <div class="notifications-wrapper">
     <a class="content" href="#">
      
       <div class="notification-item">
        <h4 class="item-title">general</h4>
        
      </div>
       
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">privacy</h4>
        
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">example1</h4>
        
      </div>
    </a>
     <a class="content" href="#">
      <div class="notification-item">
        <h4 class="item-title">example2</h4>
        
      </div>

    </a>
     

   </div>
   
    
  </ul>
  
</div>
<div class="dropdown" style="margin-right:100px;">
<a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    <i class="glyphicon glyphicon-off white"></i>
  </a>
  </div>
<div class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <img src="http://placehold.it/18x18" class="profile-image img-circle"> Username <b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li><a href="#"><i class="fa fa-cog"></i> Account</a></li>
        <li class="divider"></li>
        <li><a href="${context}/j_spring_security_logout"><i class="fa fa-sign-out"></i> Sign-out</a></li>
    </ul>
</div>
</ul><!-- /.end notification -->





        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>




<div class="row">

<div class="col-sm-2"style="padding-right:0px !important;">
<div class="navbar navbar-inverse navbar-fixed-left" style="background-color: white; border:white;">
  <a class="navbar-brand" href="#" style="background-color: #0B153E; border:white; width:100%;color:white"><center><p>Your Preferences</p></center></a>
  <ul class="nav navbar-nav">
   
   <li><a href="#" class="text-sm"  style=" color:black;"><p style="font-size:200%;color:#00CED1; font-family: Tahoma, Verdana, Segoe, sans-serif">Categories</p></a></li>
   <li><a href="#" class="text-sm" style=" color:black; padding-top:0px;"><p style="font-size:160%;" >Love-Diaries </p></a></li>
   <li><a href="#" class="text-sm" style=" color:black; padding-top:0px;"><p style="font-size:160%;">Politics</p></a></li>
   <li><a href="#" class="text-sm"  style=" color:black; padding-top:0px;"><p style="font-size:160%;">My Sins</p></a></li>
   <li><a href="#" class="text-sm" style=" color:black; padding-top:0px;"><p style="font-size:160%;">My Movies</p></a></li>
  </ul>
</div>

</div>

<div class="col-sm-8" style="padding:0px !important;" >


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="col-md-12 bootstrap snippets" style="padding:0px !important; id="ssssss">
<div class="">
<form  modelAttribute="post" id="postForm">
	<div class="" style="paading-bottom:10px;">
		<textarea name="description" path="description" class="form-control" rows="5" placeholder="What are you thinking?" style="border:2px solid black;margin-left:7px;"></textarea>
		<div class="mar-top clearfix">
			<button class="btn btn-sm btn-info pull-left" type="submit" style="border-radius:0; background:#00CED1;"><i class="fa fa-pencil fa-fw"></i> POST</button>&nbsp;&nbsp;in&nbsp;&nbsp;
			<select name="category" path="category" class="list1" style="width:160px; height:30px;">
  <option value="love">Love Diaries</option>
  <option value="like" class="option2">Like</option>
</select>
			
			
		</div>
	</div>
	</form>
</div>

<div id="GSCCModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
        <h4 class="modal-title" id="myModalLabel">Confession</h4>
      </div>
      <div class="modal-body">
        <form  modelAttribute="post" id="deleteForm">
	<div class="" style="paading-bottom:10px;">
		<input type="text" id="postId" name="id" path="id" class="form-control" />
		<h2>Are You Sure ?</h2>
		<div class="mar-top clearfix">
			<button class="btn btn-sm btn-info pull-left" type="submit" style="border-radius:0; background:#00CED1;"><i class="fa fa-pencil fa-fw"></i> Delete</button>&nbsp;&nbsp;&nbsp;&nbsp;
			
			
			
			
		</div>
	</div>
	</form>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
        <h4 class="modal-title" id="myModalLabel">Confession</h4>
      </div>
      <div class="modal-body">
        <form modelAttribute="post" id="updateForm">
	<div class="" style="paading-bottom:10px;">
		<textarea id="descriptions" name="description" path="description" class="form-control" rows="5" placeholder="description" style="border:2px solid black;margin-left:7px;"></textarea>
		<input type="text" id="postIds" name="id" path="id" class="form-control" />
		<div class="mar-top clearfix">
			<button class="btn btn-sm btn-info pull-left" type="submit" style="border-radius:0; background:#00CED1;"><i class="fa fa-pencil fa-fw"></i> SAVE CHANGES</button>
			&nbsp;&nbsp;&nbsp;&nbsp;<select path="category" name="category" class="list1" style="width:160px; height:30px;">
  <option value="love">Love Diaries</option>
  <option value="like" class="option2">Like</option>
</select>
			
			
			
		</div>
	</div>
	</form>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--====================================for each start=================================-->
<c:forEach items="${allPost}" var="element" varStatus="loop"> 
<input type="hidden" value="${element.id}" name='Status'/> 
<div class="panel results" id="result">
    <div class="panel-body">
    ${user.webUser.id}
    <!-- Newsfeed Content -->
    <!--===================================================-->
    <div class="media-block">
	<div class="media-left">
      <a  href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar1.png"></a>
	  <p><i></i><a href="#"> ${element.userId} </a></p>
	  <p class="text-info text-sm"><i class="fa fa-mobile fa-lg"></i> 11 min ago </p>
	  </div>
      <div class="media-body">
        <div class="mar-btm">
          <a href="#" class="text-sm">in <b>Love Diaries</b>  #Love Confession #true love</a>
          <c:if test="${element.userId eq user.webUser.id}">
         <span class="pull-right">
                        <a href="#" class="text-sm replyId" id="replyId" data-toggle="modal" data-target="#GSCCModal" data-id="${element.id}" data-email="${element.userId}" data-description="${element.description}"><i class="glyphicon glyphicon-edit" style="color:grey;"></i></a>
                        <a href="#" class="text-sm replyIds" id="replyId" data-toggle="modal" data-target="#GSCCModal1" data-id="${element.id}" data-email="${element.userId}" data-description="${element.description}"><i class="glyphicon glyphicon-trash"  style="color:grey; padding-right:5px;padding-left:5px;padding-bottom:2px"></i></a>
           </span></c:if>
        </div>
        <p style="background:#00CED1; color:white; padding:5px;">${element.description}</p>
        <div class="pad-ver">
          <div >
             <a  href="#"><i class="fa fa-thumbs-up" style="color:green;"><span id="${element.id}">${element.likeCount}</span></i></a>
            <a  href="#" style="padding-left:25%;"><i class="fa fa-thumbs-down" style="color:red;"><span id="${element.id}">${element.dislikeCount}</i></a>
			<span class="pull-right">
                        <p class="text-info text-sm"> 12 comment</p>
           </span>
          </div>
          
		  
        </div><br>
		 <div class="" style="background:#E5E0E0; padding:2px;">
		   <%-- <a href="#" id="like" class="text-sm" style="padding-left:5px;" data-id="${element.id}">Like</a> --%>
		    <a href="#" id="like" class="postLike" style="padding-left:5px;" data-id="${element.id}">Like</a>
		   <a href="#" id="dislike" class="postdisLike" style="padding-left:24%;" data-id="${element.id}">Dislike</a>
		   <a href="#" class="text-sm" style="padding-left:24%;"><i class="glyphicon glyphicon-comment"></i>&nbsp;Comment</a>
		   <span class="pull-right">
                        <a href="#" class="text-sm"><i class="glyphicon glyphicon-exclamation-sign"></i>&nbsp;Report Post</p></a>
           </span>
		   
		   </div>
        <hr>
				
        <!-- Comments -->
        <div>
		
		
		
			<div class="media-block">
    
    
     
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar1.png"></a>
            <div class="media-body">
             <form modelAttribute="comment" class='commentsForm' id="commentsForm${element.id}">
                <div class="input-group" style="padding-bottom:5px;"> 
                    <input name="description" path="description" class="form-control" type="text">
                    <input type="hidden"  name="id" value="${element.id}">
                    <span class="input-group-addon" style="padding:0px!important;">
                        
                        <button class="btn btn-sm pull-right csubmit" type="submit" data-id="${element.id}" id="csubmit" style="border-radius:0;;"><i class="fa fa-pencil fa-fw"></i> </button>
                    </span>
     </div>
     </form>
              </div>
              <hr>
            </div>
          </div>
		
		
		
          <div class="media-block">
		  
		  
		   
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar2.png"></a>
            <div class="media-body">
              <div class="mar-btm">
                <a href="#" class="btn-link text-semibold media-heading box-inline">Bobby Marz</a>
                <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg"></i> - From Mobile - 7 min ago</p>
              </div>
              <p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
              <div class="pad-ver">
                
		   <a href="#" class="text-sm like" style="padding-left:5px;" ">Like</a>
		   <a href="#" class="text-sm" style="padding-left:5px;">Dislike</a>
		   <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-up"></i>12</a>
		   
            <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-down"></i>2</a>
          
		   
		   </div>
                
              </div>
              <hr>
            </div>
			          <div class="media-block">
		  
		  
		   
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar2.png"></a>
            <div class="media-body">
              <div class="mar-btm">
                <a href="#" class="btn-link text-semibold media-heading box-inline">chuchu singh</a>
                <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg"></i> - From Mobile - 7 min ago</p>
              </div>
              <p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
              <div class="pad-ver">
                
		   <a href="#" class="text-sm" style="padding-left:5px;">Like</a>
		   <a href="#" class="text-sm" style="padding-left:5px;">Dislike</a>
		   <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-up"></i>12</a>
		   
            <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-down"></i>2</a>
          
		   
		   </div>
                
              </div>
              <hr>
            </div>
			
<center><a href="#" class="text-md" style="align:center;"><p style="align:center;">load more comment</p></a></center>
			
			
			
			
          </div>

         
        </div>
      </div>
    </div>
    
    
    </c:forEach>
<div class="panel">
    <div class="panel-body">
    <!-- Newsfeed Content -->
    <!--===================================================-->
    <div class="media-block">
	<div class="media-left">
      <a  href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar1.png"></a>
	  <p><i></i><a href="#"> harit kumar </a></p>
	  <p class="text-info text-sm"><i class="fa fa-mobile fa-lg"></i> 11 min ago </p>
	  </div>
      <div class="media-body">
        <div class="mar-btm">
          <a href="#" class="text-sm">in <b>Love Diaries</b>  #Love Confession #true love</a>
         
        </div>
        <p style="background:#00CED1; color:white; padding:5px;">consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
        <div class="pad-ver">
          <div >
            <a  href="#"><i class="fa fa-thumbs-up" style="color:green;">12</i></a>
            <a  href="#" style="padding-left:25%;"><i class="fa fa-thumbs-down" style="color:red;">2</i></a>
			<span class="pull-right">
                        <p class="text-info text-sm"> 12 comment</p>
           </span>
          </div>
          
		  
        </div><br>
		 <div class="" style="background:#E5E0E0; padding:2px;">
		   <a href="#" class="text-sm" style="padding-left:5px;">like</a>
		   <a href="#" class="text-sm" style="padding-left:24%;">dislike</a>
		   <a href="#" class="text-sm" style="padding-left:24%;"><i class="glyphicon glyphicon-comment"></i>&nbsp;comment</a>
		   <span class="pull-right">
                        <a href="#" class="text-sm"><i class="glyphicon glyphicon-exclamation-sign"></i>&nbsp;report</p></a>
           </span>
		   
		   </div>
        <hr>
				
        <!-- Comments -->
        <div>
		
		
		
			<div class="media-block">
    
    
     
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar1.png"></a>
            <div class="media-body">
             <form modelAttribute="comment" id="commentsForm">
                <div class="input-group" style="padding-bottom:5px;"> 
                    <input name="description" path="description" class="form-control" type="text">
                    <input type="hidden"  name="id" value="${element.id}">
                    <span class="input-group-addon" style="padding:0px!important;">
                        
                        <button class="btn btn-sm pull-right" type="submit" style="border-radius:0;;"><i class="fa fa-pencil fa-fw"></i> </button>
                    </span>
     </div>
     </form>
              </div>
              <hr>
            </div>
          </div>
		
		
		
          <div class="media-block">
		  
		  
		   
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar2.png"></a>
            <div class="media-body">
              <div class="mar-btm">
                <a href="#" class="btn-link text-semibold media-heading box-inline">Bobby Marz</a>
                <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg"></i> - From Mobile - 7 min ago</p>
              </div>
              <p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
              <div class="pad-ver">
                
		   <!-- <a href="#" class="text-sm" style="padding-left:5px;">Like</a> -->
		   <a href="#" id="like" class="postLike" style="padding-left:5px;" data-id="${element.id}">Like</a>
		   <a href="#" class="text-sm" style="padding-left:5px;">Dislike</a>
		   <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-up"></i>12</a>
		   
            <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-down"></i>2</a>
          
		   
		   </div>
                
              </div>
              <hr>
            </div>
			          <div class="media-block">
		  
		  
		   
            <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="http://bootdey.com/img/Content/avatar/avatar2.png"></a>
            <div class="media-body">
              <div class="mar-btm">
                <a href="#" class="btn-link text-semibold media-heading box-inline">chuchu singh</a>
                <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg"></i> - From Mobile - 7 min ago</p>
              </div>
              <p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
              <div class="pad-ver">
                
		   <a href="#" class="text-sm" style="padding-left:5px;">Like</a>
		   <a href="#" class="text-sm" style="padding-left:5px;">Dislike</a>
		   <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-up"></i>12</a>
		   
            <a href="#" class="text-sm" style="padding-left:5px;"><i class="fa fa-thumbs-down"></i>2</a>
          
		   
		   </div>
                
              </div>
              <hr>
            </div>
			
<center><a href="#" class="text-md" style="align:center;"><p style="align:center;">load more comment</p></a></center>
			
			
			
			
          </div>

         
        </div>
      </div>
    </div>
    <!--===================================================-->
    <!-- End Newsfeed Content -->


    <!-- Newsfeed Content -->
   
  </div>
</div>
<div class="col-sm-2">
<a class="navbar-brand" href="#" style="background-color: #0B153E; border:white; width:100%;color:white"><center><p>Classified Ads</p></center></a>
<img src="http://webneel.com/daily/sites/default/files/images/project/creative-advertisement%20(13).jpg" style="width:100%;padding-bottom:5px;padding-top:5px;">
<br>
<img src="https://31.media.tumblr.com/329ffe6c227aacad750953d7657cd3f0/tumblr_inline_ndu7ihzzKd1smnvao.jpg" style="width:100%;">

</div>
</div>

${user.webUser.email}
<script>
$(".replyId").click(function () {
	var modal = $(this);
	$("#emailM").text(modal.data('email'));
	$("#descriptions").val(modal.data('description'));
	$("#postIds").val(modal.data('id'));
});
$(".replyIds").click(function () {
	var modal = $(this);
	$("#emailM").text(modal.data('email'));
	$("#descriptions").val(modal.data('description'));
	$("#postId").val(modal.data('id'));
});
</script>

<script type="text/javascript">
$(".postLike").click(function(e){
	 e.preventDefault();
	 
	 var Status = $(this).data('id');
	 
	    $.ajax({
	        type: 'POST',
	        url:'${like}',
	        data: {
	            text: $("input[name=Status]").val(),
	            Status: Status
	        },
	        datatype : 'json',
	        success: function(data ,element) {
	         if(data=='success'){
	         var computerScore = document.getElementById(Status);
	            var number = computerScore.innerHTML;
	            number++;
	            
	            computerScore.innerHTML = number;
	         } if(data=='failure'){
	          alert(" You have already liked it");
	         }

	        }
	    });
	        
	});

$(".postdisLike").click(function(){
	
	var Status = $(this).data('id');
	
    $.ajax({
        type: 'POST',
        url:'${dislike}',
        data: {
            text: $("input[name=Status]").val(),
            Status: Status
        },
        datatype : 'json',
        success: function(data ,element) {
        	if(data=='success'){
        	var computerScore = document.getElementById($("input[name=Status]").val());
            var number = computerScore.innerHTML;
            number++;
            
            computerScore.innerHTML = number;
        	} if(data=='failure'){
        		alert(" You have already disliked it");
        	}

        }
    });
        
});
    
</script>
<script>
$(document).ready(function(){


$("#postForm").submit(function(e) {
	 e.preventDefault();
   // var url = "path/to/your/script.php";// the script where you handle the form input.

    $.ajax({
           type: "POST",
           url:'${post}',
           data: $("#postForm").serialize(), // serializes the form's elements.
           datatype : 'json',
           success: function(result)
           {
        	   location.reload();
              
           }
    
         })
   });

    // avoid to execute the actual submit of the form.
    
    $("#updateForm").submit(function(e) {
	 e.preventDefault();
   // var url = "path/to/your/script.php";// the script where you handle the form input.

    $.ajax({
           type: "POST",
           url:'${upost}',
           data: $("#updateForm").serialize(), // serializes the form's elements.
           datatype : 'json',
           success: function(data)
           {
        	   location.reload();
               //var parsed = $.parseHTML(data);
               //result = $(parsed).find("#result");
               //$("#result").load(location.href+" #result>*","");
           }
    
         });
   });

    // avoid to execute the actual submit of the form.
    $("#deleteForm").submit(function(e) {
   	 e.preventDefault();
      // var url = "path/to/your/script.php";// the script where you handle the form input.

       $.ajax({
              type: "POST",
              url:'${dpost}',
              data: $("#deleteForm").serialize(), // serializes the form's elements.
              datatype : 'json',
              success: function(data)
              {
                  
            	  location.reload();
              }
       
            });
      });

    
    
});

$('.csubmit').click(function(e) {
    
    e.preventDefault();
     // var url = "path/to/your/script.php";// the script where you handle the form input.
    var cd = $(this).data('id');
     
      $.ajax({
             type: "POST",
             url:'${commen}',
             
             data: $('#commentsForm'+cd).serialize(), // serializes the form's elements.
             datatype : 'json',
             success: function(data)
             {
              location.reload();
                 //var parsed = $.parseHTML(data);
                 //result = $(parsed).find("#result");
                 //$("#result").load(location.href+" #result>*","");
             }
      
           });
     });

</script>
<script src="${context}/resources/bootstrap.min.js"></script>

</body>
</html>