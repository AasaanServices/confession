/*package com.confession.web.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.confession.web.entity.WebUser;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Component
public class UserWriterConverter implements Converter<WebUser, DBObject> {

    @Override
    public DBObject convert(final WebUser user) {
        final DBObject dbObject = new BasicDBObject();
        dbObject.put("name", user.getName());
        dbObject.put("age", user.getAge());
        if (user.getRoles()() != null) {
            final DBObject emailDbObject = new BasicDBObject();
            emailDbObject.put("value", user.getEmailAddress().getValue());
            dbObject.put("email", emailDbObject);
        }
        dbObject.removeField("_class");
        return dbObject;
    }

}
*/