package com.confession.web.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.confession.web.dao.UserDao;
import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
import com.confession.web.model.LoginUser;
import com.confession.web.service.UserService;

@Component
public class CustomUserDetailsService implements UserDetailsService

{

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String userName)

	throws UsernameNotFoundException {

		WebUser appUser = null;
		LoginUser loginUser = null;
		
			try {
				//logger.info("Authenticating User : " + username);
				appUser = userDao.getUserByEmail(userName);
			} catch (Exception ex) {
				//logger.error("AuthenticationService --> loadUserByUsername -->" +  ExceptionUtils.getStackTrace(ex));
				return loginUser;
			}
			
			if(null == appUser){
				 return loginUser;
			} else {
				List<String> roles = new ArrayList<String>();
				for (Role userProfile : appUser.getRoles()) {
					roles.add(userProfile.getName());
				}

				loginUser = new LoginUser(appUser.getEmail(), appUser.getPassword(), appUser.isEnabled(), appUser.isAccountNonExpired(), appUser.isCredentialsNonExpired(), appUser.isAccountNonLocked(),
						getGrantedAuthorities(roles), appUser);
			}
			
			return loginUser;

	}
	
	public static List<GrantedAuthority> getGrantedAuthorities(final List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

}