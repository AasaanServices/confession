package com.confession.web.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.confession.web.Constant.ServerConstant;
import com.confession.web.dao.PostDao;
import com.confession.web.entity.Dislikes;
import com.confession.web.entity.Likes;
import com.confession.web.entity.Post;
import com.confession.web.util.IPostRepository;

//@Transactional
@Repository
public class PostDaoImpl implements PostDao {
	
	 @Autowired
	    private IPostRepository postRepository;
	
	
	
	

	@Override
	public Post savePost(Post post) throws Exception {
		try{
			postRepository.save(post);
		} catch(Exception e){
			
			throw new Exception(e.getMessage());
		}
		return post;
	}

	@Override
	public List<Post> getAllPost() throws Exception {
		
		return postRepository.findAll();
	}
	
	@Override
	public List<Post> getAllActivePost() throws Exception {
		return postRepository.getAllActivePost(new Sort(Sort.Direction.DESC, "lastModifiedDate"));
	}




	@Override
	public Post updatePost(Post post,String status) throws Exception {
		try{
			Post existingPost= postRepository.findOne(post.getId());
			existingPost.setDescription(post.getDescription());
			existingPost.setActive(status);
			existingPost.setLastModifiedDate(post.getLastModifiedDate());
			postRepository.save(existingPost);
		} catch(Exception e){
			//logger.error("AppUserDaoImpl --> updateUserProfile -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return post;
	}



	@Override
	public Post getPostByid(String id) throws Exception {
		return postRepository.findOne(id);
	}



	@Override
	public void deletePost(String id) throws Exception {
		postRepository.delete(id);
	}



	


	@Override
	public Likes getLikeByUserIdAndPostId(String postId, String userId) throws Exception {
		return postRepository.getLikeByUserIdAndPostId(postId, userId);
	}
	
	@Override
	public Dislikes getdisLikeByUserIdAndPostId(String postId, String userId) throws Exception {
		return postRepository.getdisLikeByUserIdAndPostId(postId, userId);
	}
	

}
