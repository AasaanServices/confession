package com.confession.web.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.confession.web.convertor.UserConvertor;
import com.confession.web.dto.WebUserDTO;
import com.confession.web.entity.Post;
import com.confession.web.entity.WebUser;
import com.confession.web.service.PostService;
import com.confession.web.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private PostService postService;
	@Autowired
	private UserService service;
	@Autowired
	private UserConvertor userConvertor;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	@RequestMapping(value = "/u/dashboard", method = RequestMethod.GET)
	public String main(@ModelAttribute Post post,final HttpServletRequest request, final Model model) {
		//WebUser appUser = getUserInfo(request).getWebUser();
		try {
			
			model.addAttribute("allPost", postService.getAllActivePost() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "main";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUp(@ModelAttribute WebUserDTO webUserDTO,final HttpServletRequest request, final Model model) {
		//WebUser appUser = getUserInfo(request).getWebUser();
		
		
		
		return "signup";
	}
	
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public String home(@ModelAttribute WebUserDTO webUserDTO,Locale locale, Model model) {
		
		
		try {
			WebUser user=userConvertor.userDtoToEntity(webUserDTO);
			service.saveUser(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "login";
	}
	
}
