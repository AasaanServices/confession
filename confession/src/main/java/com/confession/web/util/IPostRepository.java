////////////////////////////////////////////////////////////////////////////////
// $$Id: $$
// $$Change: $$
// $$DateTime: $$
// $$Author: $$
//
// Copyright (c) 2006 - 2016 FORCAM GmbH. All rights reserved.
////////////////////////////////////////////////////////////////////////////////
package com.confession.web.util;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.confession.web.entity.Dislikes;
import com.confession.web.entity.Likes;
import com.confession.web.entity.Post;


@Repository(value = "postRepository")
public interface IPostRepository extends MongoRepository<Post,String> {
	
	@Query("{'id' : ?0,'like.userId' : ?1}")
	Likes getLikeByUserIdAndPostId(String postId,String UserId) throws Exception;
	
	@Query("{'id' : ?0,'dislike.userId' : ?1}")
	Dislikes getdisLikeByUserIdAndPostId(String postId,String UserId) throws Exception;
	@Query("{'active' : 'A'}")
	List<Post> getAllActivePost(Sort sort) throws Exception;
	

}
