package com.confession.web.util;

import org.apache.commons.codec.binary.Base64;

public class Encryption {

	public static String easeyEncrypt(String str) {
		byte[] bytesEncoded = Base64.encodeBase64(str.getBytes());
		return new String(bytesEncoded);
	}
	
	public static String easeyDecrypt(String str) {
		byte[] bytesEncoded = Base64.decodeBase64(str.getBytes());
		return new String(bytesEncoded);
	}
}