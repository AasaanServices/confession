////////////////////////////////////////////////////////////////////////////////
// $$Id: $$
// $$Change: $$
// $$DateTime: $$
// $$Author: $$
//
// Copyright (c) 2006 - 2016 FORCAM GmbH. All rights reserved.
////////////////////////////////////////////////////////////////////////////////
package com.confession.web.util;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;


@Repository(value = "userRepository")
public interface IUserRepository extends MongoRepository<WebUser, String> {
	
	
	@Query("{'role.name' : ?0}")
	List<Role> getUserProfileByRoleName(String roleName) throws Exception;
	
	@Query("{'email' : ?0}")
	List<WebUser> getUserByEmail(String email) throws Exception;

}
