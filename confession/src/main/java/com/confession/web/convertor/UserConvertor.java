package com.confession.web.convertor;

import java.util.HashSet;
import java.util.Set;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.confession.web.Constant.ServerConstant;
import com.confession.web.dto.WebUserDTO;
import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
import com.confession.web.enums.UserProfileTypeEnum;
import com.confession.web.service.UserService;

@Component
public class UserConvertor {
	
	Role roleP = new Role();
	
	@Autowired
	UserService userServiceImpl;
	
	/*@Autowired
	private BCryptPasswordEncoder encoder;*/
	
	
	
	public WebUser userDtoToEntity(WebUserDTO userDto){
		WebUser user=null;
		user=new WebUser();
		user.setName(userDto.getFirstName()+" "+userDto.getLastName());
		user.setEmail(userDto.getEmail());
		String p=userDto.getPassword();
		//user.setPassword(encoder.encode(p));
		user.setPassword(p);
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setCreatedDate(new LocalDateTime());
		user.setLastModifiedDate(new LocalDateTime());
		user.setActive(ServerConstant.ACTIVE);
		
		user.setSignIn(false);
		user.setAccountNonExpired(true);
		
		user.setCredentialsNonLocked(true);
		user.setEnabled(true);
		user.setCredentialsNonExpired(true);
		user.setPhoneVerified(true);
		user.setAccountNonLocked(true);
	
		
		
		Role role = null;
		try {
			role = userServiceImpl.getUserProfileByRoleName(UserProfileTypeEnum.ROLE_USER.name());
		} catch (Exception e) {
			//log.error("SuperAdminConvertor --> createUserProfile -->" + ExceptionUtils.getStackTrace(e));
		}
		Set<Role> roles = new HashSet<Role>();
		
		if (null != role) {
			roles.add(role);
			user.setRoles(roles);
		} else {
			
			//createUserProfile();
			roleP.setId("2");
			roles.add(roleP);
			user.setRoles(roles);
			
		}
		
		return user;
	}
	public WebUserDTO webUserDtoToEntity(WebUserDTO webUserDto, WebUser user) {
		webUserDto.setFirstName(user.getFirstName());
		webUserDto.setLastName(user.getLastName());
		webUserDto.setEmail(user.getEmail());
		
		webUserDto.setStatus(user.getActive());
		webUserDto.setId(user.getId());
		
		return webUserDto;
	}
	public WebUser adminUserDtoToEntity(WebUserDTO userDto){
		WebUser user=null;

		
		if(userDto.getAction().equalsIgnoreCase("c")){
			user=new WebUser();
			//user.setCraetedDate(new LocalDateTime());
			//user.setLastModifiedDate(new LocalDateTime());
		}else{
			try {
				user = userServiceImpl.getUserById(userDto.getId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//user=new WebUser();
		user.setName(userDto.getFirstName()+" "+userDto.getLastName());
		user.setEmail(userDto.getEmail());
		String p=userDto.getPassword();
		//user.setPassword(encoder.encode(p));
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		
		user.setActive(userDto.getStatus());
		
		user.setSignIn(false);
		user.setAccountNonExpired(true);
		
		user.setCredentialsNonLocked(true);
		user.setEnabled(true);
		user.setCredentialsNonExpired(true);
		user.setPhoneVerified(true);
		user.setAccountNonLocked(true);
	
		
		
		Role role = null;
		try {
			role = userServiceImpl.getUserProfileByRoleName(UserProfileTypeEnum.ROLE_GOLDUSER.name());
		} catch (Exception e) {
			//log.error("SuperAdminConvertor --> createUserProfile -->" + ExceptionUtils.getStackTrace(e));
		}
		Set<Role> roles = new HashSet<Role>();
		
		if (null != role) {
			roles.add(role);
			user.setRoles(roles);
		} else {
			
			//createUserProfile1();
			roles.add(roleP);
			user.setRoles(roles);
			
		}
		
		return user;
	}
	/*public void createUserProfile() {
		try {
			Role userProfile = new Role();
			userProfile.setCraetedDate(new LocalDateTime());
			userProfile.setLastModifiedDate(new LocalDateTime());
			userProfile.setType(UserProfileTypeEnum.ROLE_USER.name());
			roleP = userServiceImpl.saveUserProfile(userProfile);
		} catch (Exception e) {
			//log.error("SuperAdminConvertor --> createUserProfile -->" + ExceptionUtils.getStackTrace(e));
		}
	}*/
	/*public void createUserProfile1() {
		try {
			Role userProfile = new Role();
			
			userProfile.setType(UserProfileTypeEnum.ROLE_GOLDUSER.name());
			userProfile.setCraetedDate(new LocalDateTime());
			userProfile.setLastModifiedDate(new LocalDateTime());
			roleP = userServiceImpl.saveUserProfile(userProfile);
		} catch (Exception e) {
			//log.error("SuperAdminConvertor --> createUserProfile -->" + ExceptionUtils.getStackTrace(e));
		}
	}*/
}
