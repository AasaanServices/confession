/**
 *  Copyright 2015 Mutation Labs . All Rights Reserved.
 *  Mutation Labs PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.confession.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Override UsernamePasswordAuthenticationFilter to create our own custom Authentication filter.
 * 
 * @since 20-05-2015
 * @author haritkumar
 */
public class AssistUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
    public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";

    private final String       usernameParameter                 = SPRING_SECURITY_FORM_USERNAME_KEY;
    private final String       passwordParameter                 = SPRING_SECURITY_FORM_PASSWORD_KEY;

    /**
     * This method is used to obtain user name from request.
     * 
     * @param request HttpServletRequest
     * @return String username
     */
    @Override
    protected String obtainUsername(final HttpServletRequest request) {
        final String username = request.getParameter(usernameParameter);
        final HttpSession session = request.getSession();
        session.setAttribute("username", username);
        return username.toLowerCase().trim();
    }

    /**
     * This method is used to obtain user password from request.
     * 
     * @param request HttpServletRequest
     * @return String password
     */
    @Override
    protected String obtainPassword(final HttpServletRequest request) {
        return request.getParameter(passwordParameter);
    }

    /**
     * attemptAuthentication method to call super method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return Authentication Authentication
     * @throws AuthenticationException AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) throws AuthenticationException {
        return super.attemptAuthentication(request, response);
    }

}
