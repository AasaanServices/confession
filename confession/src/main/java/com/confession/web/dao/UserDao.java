package com.confession.web.dao;

import java.util.List;

import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
public interface UserDao {
	WebUser saveUser(WebUser user) throws Exception;
	//Role saveUserProfile(Role userProfile) throws Exception;
	Role getUserProfileByRoleName(String roleName) throws Exception;
	WebUser getUserByEmail(String email) throws Exception;
	//Role updateUserProfile(Role role) throws Exception;
	WebUser updateUser(WebUser user) throws Exception;
	List<WebUser> getAllUser() throws Exception;
	WebUser getUserByid(String id) throws Exception;

}
