package com.confession.web.dao;

import java.util.List;

import com.confession.web.entity.Dislikes;
import com.confession.web.entity.Likes;
import com.confession.web.entity.Post;

public interface PostDao {
	
	Post savePost(Post post) throws Exception;
	
	Post updatePost(Post post,String status) throws Exception;
	
	List<Post> getAllPost() throws Exception;
	
	List<Post> getAllActivePost() throws Exception;
	
	Post getPostByid(String id) throws Exception;
	
	void deletePost(String id) throws Exception;
	
	/*Likes saveLike(Likes likes) throws Exception;
	
	Dislikes savedisLike(Dislikes likes) throws Exception;*/
	
	Likes getLikeByUserIdAndPostId(String postId,String UserId) throws Exception;
	
	Dislikes getdisLikeByUserIdAndPostId(String postId,String UserId) throws Exception;

}
