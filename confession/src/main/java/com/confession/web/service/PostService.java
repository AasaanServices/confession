package com.confession.web.service;

import java.util.List;

import com.confession.web.entity.Dislikes;
import com.confession.web.entity.Likes;
import com.confession.web.entity.Post;

public interface PostService {
	Post savePost(Post Post) throws Exception;
	Post updatePost(Post Post,String status) throws Exception;
	List<Post> getAllPost() throws Exception;
	
	List<Post> getAllActivePost() throws Exception;
	Post getPostById(String id) throws Exception;
	void deletePost(Post post) throws Exception;
	/*Likes saveLike(Likes likes) throws Exception;
	
	Dislikes savedisLike(Dislikes likes) throws Exception;*/
	Likes getLikeByUserIdAndPostId(String postId,String userId) throws Exception;
	
	Dislikes getdisLikeByUserIdAndPostId(String postId,String userId) throws Exception;

}
