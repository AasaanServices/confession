
package com.confession.web.entity;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "user")
public class User {

    @Id
    private String user_id;

    private String user_name;

    private String password;

    private String dob;

    private String email_id;

    private String mobile_no;

    private String address;

    private String role;

    private String department;

    private String reg_date;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String mPassword) {
        this.password = mPassword;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String mDob) {
        this.dob = mDob;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String mEmail_id) {
        this.email_id = mEmail_id;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mMobile_no) {
        this.mobile_no = mMobile_no;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String mAddress) {
        this.address = mAddress;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String mRole) {
        this.role = mRole;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String mDepartment) {
        this.department = mDepartment;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String mReg_date) {
        this.reg_date = mReg_date;
    }

}
