package com.confession.web.entity;

import java.util.Set;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "post")
public class Post extends BaseEntity{
	
	@Id
	private String id;
	
	private String description;
	
	private String category;
	
	
	private Set<Likes> like;
	
	private String likeCount;
	
	private String dislikeCount;
	
	private Set<Dislikes> dislike;
	
	private Set<Comment> comment;
	
	private Set<Report> report;
	
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	

	public Set<Likes> getLike() {
		return like;
	}

	public void setLike(Set<Likes> like) {
		this.like = like;
	}

	
	public Set<Dislikes> getDislike() {
		return dislike;
	}

	public void setDislike(Set<Dislikes> dislike) {
		this.dislike = dislike;
	}



	public Set<Comment> getComment() {
		return comment;
	}

	public void setComment(Set<Comment> comment) {
		this.comment = comment;
	}

	public Set<Report> getReport() {
		return report;
	}

	public void setReport(Set<Report> report) {
		this.report = report;
	}

	

	public String getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(String likeCount) {
		this.likeCount = likeCount;
	}

	public String getDislikeCount() {
		return dislikeCount;
	}

	public void setDislikeCount(String dislikeCount) {
		this.dislikeCount = dislikeCount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	

}
