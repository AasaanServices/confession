package com.confession.web.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import com.confession.web.enums.UserProfileTypeEnum;


//@Document(collection = "role")
public class Role extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6140975500756993111L;

	@Id
    private String id;
	
    private String name = UserProfileTypeEnum.ROLE_USER.getUserProfileType();
    
     
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
 

}
