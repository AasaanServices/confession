package com.confession.web.entity;

import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//@Document(collection = "comment")
public class Comment extends BaseEntity {
	
	@Id
	private String id;
	
	private String count;
	
	private String description;
	
	private String userId;
	
	private Set<Likes> like;
	
	private String liked;
	
	private Set<Dislikes> dislike;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Set<Likes> getLike() {
		return like;
	}

	public void setLike(Set<Likes> like) {
		this.like = like;
	}

	public String getLiked() {
		return liked;
	}

	public void setLiked(String liked) {
		this.liked = liked;
	}

	public Set<Dislikes> getDislike() {
		return dislike;
	}

	public void setDislike(Set<Dislikes> dislike) {
		this.dislike = dislike;
	}
	

	
	
	

}
