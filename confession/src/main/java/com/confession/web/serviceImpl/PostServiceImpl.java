package com.confession.web.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.confession.web.dao.PostDao;
import com.confession.web.entity.Dislikes;
import com.confession.web.entity.Likes;
import com.confession.web.entity.Post;
import com.confession.web.service.PostService;

@Service
public class PostServiceImpl implements PostService{
	
	@Autowired
	private PostDao postDao;

	@Override
	public Post savePost(Post post) throws Exception {
		Post postp = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			postp = postDao.savePost(post);
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return postp;
	}

	@Override
	public List<Post> getAllPost() throws Exception {
		List<Post> postp = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			postp = postDao.getAllPost();
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return postp;
	}
	
	
	@Override
	public List<Post> getAllActivePost() throws Exception {
		List<Post> postp = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			postp = postDao.getAllActivePost();
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return postp;
	}

	@Override
	public Post updatePost(Post post,String status) throws Exception {
		Post postp = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			postp = postDao.updatePost(post,status);
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return postp;
	}

	@Override
	public Post getPostById(String id) throws Exception {
		Post post = null;
		try{
			post = postDao.getPostByid(id);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return post;
	}

	@Override
	public void deletePost(Post post) throws Exception {
		 postDao.deletePost(post.getId());
		
		
	}

	/*@Override
	public Likes saveLike(Likes likes) throws Exception {
		Likes like = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			like = postDao.saveLike(likes);
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return like;
	}*/
	
	/*@Override
	public Dislikes savedisLike(Dislikes dislikes) throws Exception {
		Dislikes dislike = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			dislike = postDao.savedisLike(dislikes);
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return dislike;
	}*/

	@Override
	public Likes getLikeByUserIdAndPostId(String postId, String userId) throws Exception {
		Likes like = null;
		try{
			like = postDao.getLikeByUserIdAndPostId(postId,userId);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return like;
	}
	
	
	@Override
	public Dislikes getdisLikeByUserIdAndPostId(String postId, String userId) throws Exception {
		Dislikes dislike = null;
		try{
			dislike = postDao.getdisLikeByUserIdAndPostId(postId,userId);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return dislike;
	}

	
}
