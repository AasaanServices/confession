package com.confession.web.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.confession.web.dao.UserDao;
import com.confession.web.entity.Post;
import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
import com.confession.web.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UserDao appUserDao;
	
	@Override
	public WebUser saveUser(WebUser appUser) throws Exception {
		WebUser appUserP = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			appUserP = appUserDao.saveUser(appUser);
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return appUserP;
	}

	/*@Override
	public Role saveUserProfile(Role userProfile) throws Exception {
		Role roleP = null;
		try{
			
			roleP = appUserDao.saveUser(new WebUser().);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> saveUserProfile -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return roleP;
	}*/
	@Override
	public Role getUserProfileByRoleName(String roleName) throws Exception {
		Role roleP = null;
		try{
			roleP = appUserDao.getUserProfileByRoleName(roleName);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return roleP;
	}
	public WebUser getUserByEmail(String email) throws Exception {
		WebUser roleP = null;
		try{
			roleP = appUserDao.getUserByEmail(email);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return roleP;
	}

	/*@Override
	public Role updateUserProfile(Role userProfile) throws Exception {
		Role roleP = null;
		try{
			roleP = appUserDao.updateUserProfile(userProfile);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> updateUserProfile -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return roleP;
	}*/

	@Override
	public WebUser updateUser(WebUser user) throws Exception {
		WebUser webUser = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			webUser = appUserDao.updateUser(user);
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return webUser;
	}

	@Override
	public List<WebUser> getAllUser() throws Exception {
		List<WebUser> user = null;
		try{
			//String pass = appUser.getPassword();
			//appUser.setPassword(encoder.encode(appUser.getPassword()));
			user = appUserDao.getAllUser();
			
			
		} catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return user;
	}

	@Override
	public WebUser getUserById(String id) throws Exception {
		WebUser user = null;
		try{
			user = appUserDao.getUserByid(id);
		} catch(Exception e){
			//logger.error("AppUserServiceImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return user;
	}

}
