package com.confession.web.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.confession.web.dao.UserDao;
import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
import com.confession.web.model.LoginUser;




/**
 * @author haritkumar
 */
@Service("authenticationService")
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	

	 public void setUserDao(final UserDao userDao) {
	        this.userDao = userDao;
	    }
	/**
	 * @param username the username identifying the user whose data is required.
	 * @return a fully populated user record (never <code>null</code>)
	 * @throws UsernameNotFoundException if the user could not be found or the user has no GrantedAuthority
	 */
	 @Override
		public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
			WebUser appUser = null;
			LoginUser loginUser = null;
			
				try {
					//logger.info("Authenticating User : " + username);
					appUser = userDao.getUserByEmail(email);
				} catch (Exception ex) {
					//logger.error("AuthenticationService --> loadUserByUsername -->" +  ExceptionUtils.getStackTrace(ex));
					return loginUser;
				}
				
				if(null == appUser){
					 return loginUser;
				} else {
					List<String> roles = new ArrayList<String>();
					for (Role userProfile : appUser.getRoles()) {
						roles.add(userProfile.getName());
					}

					loginUser = new LoginUser(appUser.getEmail(), appUser.getPassword(), appUser.isEnabled(), appUser.isAccountNonExpired(), appUser.isCredentialsNonExpired(), appUser.isAccountNonLocked(),
							getGrantedAuthorities(roles), appUser);
				}
				
				return loginUser;
		}

	public static List<GrantedAuthority> getGrantedAuthorities(final List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
}
